/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author eduardo.impellizzere
 */
public class ProductoControladorTest {
    
    public ProductoControladorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of controlarStock method, of class ProductoControlador.
     */
    @Test
    public void testControlarStock() {
        Generador.datos = new Generador().generarDatos();
        
        assertTrue(new ProductoControlador().controlarStock(1));
        assertFalse(new ProductoControlador().controlarStock(2));
        assertTrue(new ProductoControlador().controlarStock(3));
    }

    /**
     * Test of agregarProducto method, of class ProductoControlador.
     */
    @Test
    public void testAgregarProducto() {
        Generador.datos = new Generador().generarDatos();
        
        new ProductoControlador().agregarProducto(1,5);
        new ProductoControlador().agregarProducto(2,9);
        new ProductoControlador().agregarProducto(3,0);
        
        String expected = "LineaPedido{idLineaPedido=0, cantidadLineaPedido=0, LineasPedido=[Producto{idProducto=1, nombreProducto=Milanesa, descripcionProducto=Sandwich de Milanesa, codigoProducto=1, stockProducto=50, precioProducto=80.0, rubroProducto=Rubro{idRubro=1, descripcionRubro=Sandwich}, AgregadosProducto=[]}, Producto{idProducto=2, nombreProducto=Lomito, descripcionProducto=Sandwich de Lomito, codigoProducto=2, stockProducto=0, precioProducto=100.0, rubroProducto=Rubro{idRubro=1, descripcionRubro=Sandwich}, AgregadosProducto=[]}, Producto{idProducto=3, nombreProducto=Hamburguesa, descripcionProducto=Hamburguesa, codigoProducto=3, stockProducto=50, precioProducto=80.0, rubroProducto=Rubro{idRubro=1, descripcionRubro=Sandwich}, AgregadosProducto=[]}]}";
        Assert.assertEquals(expected, Generador.lineaPedido.toString());
    }
    
}
