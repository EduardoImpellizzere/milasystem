/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author eduardo.impellizzere
 */
public class RubroTest {
    
    public RubroTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of toString method, of class Rubro.
     */
    @Test
    public void testToString() {
        Rubro rubro = new Rubro(1,"Papas");
        String expected = "Rubro{idRubro=1, descripcionRubro=Papas}";
        Assert.assertEquals(expected, rubro.toString());
    }

    /**
     * Test of getIdRubro method, of class Rubro.
     */
    @Test
    public void testGetIdRubro() {
        Rubro rubro = new Rubro();
        rubro.setIdRubro(2);
        assertTrue(rubro.getIdRubro() == 2);
    }

    /**
     * Test of setIdRubro method, of class Rubro.
     */
    @Test
    public void testSetIdRubro() {
        Rubro rubro = new Rubro();
        rubro.setIdRubro(4);
        assertTrue(rubro.getIdRubro() == 4);
    }

    /**
     * Test of getDescripcionRubro method, of class Rubro.
     */
    @Test
    public void testGetDescripcionRubro() {
        Rubro rubro = new Rubro();
        String expected = "Rubro test 1";
        rubro.setDescripcionRubro(expected);
        assertTrue(rubro.getDescripcionRubro().equals(expected));
    }

    /**
     * Test of setDescripcionRubro method, of class Rubro.
     */
    @Test
    public void testSetDescripcionRubro() {
        Rubro rubro = new Rubro();
        String expected = "Rubro test 10000000000";
        rubro.setDescripcionRubro(expected);
        assertTrue(rubro.getDescripcionRubro().equals(expected));
    }
    
}
