/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author eduardo.impellizzere
 */
public class Cliente extends Persona{
    private int idCliente;
    private Documento documentoCliente;
    private CondicionTributaria condicionTributariaCliente;

    @Override
    public String toString() {
        return "Cliente{" + "idCliente=" + idCliente + ", documentoCliente=" + documentoCliente + ", condicionTributariaCliente=" + condicionTributariaCliente + '}';
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public Documento getDocumentoCliente() {
        return documentoCliente;
    }

    public void setDocumentoCliente(Documento documentoCliente) {
        this.documentoCliente = documentoCliente;
    }

    public CondicionTributaria getCondicionTributariaCliente() {
        return condicionTributariaCliente;
    }

    public void setCondicionTributariaCliente(CondicionTributaria condicionTributariaCliente) {
        this.condicionTributariaCliente = condicionTributariaCliente;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getApellidoPersona() {
        return apellidoPersona;
    }

    public void setApellidoPersona(String apellidoPersona) {
        this.apellidoPersona = apellidoPersona;
    }

    public String getTelefonoPersona() {
        return telefonoPersona;
    }

    public void setTelefonoPersona(String telefonoPersona) {
        this.telefonoPersona = telefonoPersona;
    }

    public String getDomicilioPersona() {
        return domicilioPersona;
    }

    public void setDomicilioPersona(String domicilioPersona) {
        this.domicilioPersona = domicilioPersona;
    }

    public Cliente(int idCliente, Documento documentoCliente, CondicionTributaria condicionTributariaCliente, String nombrePersona, String apellidoPersona, String telefonoPersona, String domicilioPersona) {
        super(nombrePersona, apellidoPersona, telefonoPersona, domicilioPersona);
        this.idCliente = idCliente;
        this.documentoCliente = documentoCliente;
        this.condicionTributariaCliente = condicionTributariaCliente;
    }
}
