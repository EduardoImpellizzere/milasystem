/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author eduardo.impellizzere
 */
public class Documento {
    private int idDocumento;
    private String nombreDocumento;
    private int codigoDocumento;
    private int numeroDocumento;

    @Override
    public String toString() {
        return "Documento{" + "idDocumento=" + idDocumento + ", nombreDocumento=" + nombreDocumento + ", codigoDocumento=" + codigoDocumento + ", numeroDocumento=" + numeroDocumento + '}';
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public int getCodigoDocumento() {
        return codigoDocumento;
    }

    public void setCodigoDocumento(int codigoDocumento) {
        this.codigoDocumento = codigoDocumento;
    }

    public int getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(int numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Documento(int idDocumento, String nombreDocumento, int codigoDocumento, int numeroDocumento) {
        this.idDocumento = idDocumento;
        this.nombreDocumento = nombreDocumento;
        this.codigoDocumento = codigoDocumento;
        this.numeroDocumento = numeroDocumento;
    }
}
