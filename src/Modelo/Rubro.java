/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author eduardo.impellizzere
 */
public class Rubro {
    private int idRubro;
    private String descripcionRubro;

    public Rubro() {
    }

    @Override
    public String toString() {
        return "Rubro{" + "idRubro=" + idRubro + ", descripcionRubro=" + descripcionRubro + '}';
    }

    public int getIdRubro() {
        return idRubro;
    }

    public void setIdRubro(int idRubro) {
        this.idRubro = idRubro;
    }

    public String getDescripcionRubro() {
        return descripcionRubro;
    }

    public void setDescripcionRubro(String descripcionRubro) {
        this.descripcionRubro = descripcionRubro;
    }

    public Rubro(int idRubro, String descripcionRubro) {
        this.idRubro = idRubro;
        this.descripcionRubro = descripcionRubro;
    }
}
