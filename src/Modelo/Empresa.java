/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author eduardo.impellizzere
 */
public class Empresa {
    private CondicionTributaria condicionTributariaEmpresa;
    private ArrayList<PuntoVenta> PuntosVentaEmpresa = new ArrayList<>();

    @Override
    public String toString() {
        return "Empresa{" + "condicionTributariaEmpresa=" + condicionTributariaEmpresa + ", PuntosVentaEmpresa=" + PuntosVentaEmpresa + '}';
    }

    public CondicionTributaria getCondicionTributariaEmpresa() {
        return condicionTributariaEmpresa;
    }

    public void setCondicionTributariaEmpresa(CondicionTributaria condicionTributariaEmpresa) {
        this.condicionTributariaEmpresa = condicionTributariaEmpresa;
    }

    public ArrayList<PuntoVenta> getPuntosVentaEmpresa() {
        return PuntosVentaEmpresa;
    }

    public void setPuntosVentaEmpresa(ArrayList<PuntoVenta> PuntosVentaEmpresa) {
        this.PuntosVentaEmpresa = PuntosVentaEmpresa;
    }

    public Empresa(CondicionTributaria condicionTributariaEmpresa) {
        this.condicionTributariaEmpresa = condicionTributariaEmpresa;
    }
}
