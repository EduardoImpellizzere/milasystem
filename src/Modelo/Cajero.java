/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author eduardo.impellizzere
 */
public class Cajero extends Persona{
    private int idCajero;
    private int legajoCajero;
    private String claveCajero;
    private Turno turnoCajero;

    @Override
    public String toString() {
        return "Cajero{" + "idCajero=" + idCajero + ", legajoCajero=" + legajoCajero + ", claveCajero=" + claveCajero + ", turnoCajero=" + turnoCajero + '}';
    }

    public int getIdCajero() {
        return idCajero;
    }

    public void setIdCajero(int idCajero) {
        this.idCajero = idCajero;
    }

    public int getLegajoCajero() {
        return legajoCajero;
    }

    public void setLegajoCajero(int legajoCajero) {
        this.legajoCajero = legajoCajero;
    }

    public String getClaveCajero() {
        return claveCajero;
    }

    public void setClaveCajero(String claveCajero) {
        this.claveCajero = claveCajero;
    }

    public Turno getTurnoCajero() {
        return turnoCajero;
    }

    public void setTurnoCajero(Turno turnoCajero) {
        this.turnoCajero = turnoCajero;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getApellidoPersona() {
        return apellidoPersona;
    }

    public void setApellidoPersona(String apellidoPersona) {
        this.apellidoPersona = apellidoPersona;
    }

    public String getTelefonoPersona() {
        return telefonoPersona;
    }

    public void setTelefonoPersona(String telefonoPersona) {
        this.telefonoPersona = telefonoPersona;
    }

    public String getDomicilioPersona() {
        return domicilioPersona;
    }

    public void setDomicilioPersona(String domicilioPersona) {
        this.domicilioPersona = domicilioPersona;
    }

    public Cajero(int idCajero, int legajoCajero, String claveCajero, Turno turnoCajero, String nombrePersona, String apellidoPersona, String telefonoPersona, String domicilioPersona) {
        super(nombrePersona, apellidoPersona, telefonoPersona, domicilioPersona);
        this.idCajero = idCajero;
        this.legajoCajero = legajoCajero;
        this.claveCajero = claveCajero;
        this.turnoCajero = turnoCajero;
    }
}
