/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author eduardo.impellizzere
 */
public class PuntoVenta {
    private int idPuntoVenta;
    private String nombrePuntoVenta;
    private String direccionPuntoVenta;
    private ArrayList<Pedido> Pedidos = new ArrayList<>();

    @Override
    public String toString() {
        return "PuntoVenta{" + "idPuntoVenta=" + idPuntoVenta + ", nombrePuntoVenta=" + nombrePuntoVenta + ", direccionPuntoVenta=" + direccionPuntoVenta + ", Pedidos=" + Pedidos + '}';
    }

    public int getIdPuntoVenta() {
        return idPuntoVenta;
    }

    public void setIdPuntoVenta(int idPuntoVenta) {
        this.idPuntoVenta = idPuntoVenta;
    }

    public String getNombrePuntoVenta() {
        return nombrePuntoVenta;
    }

    public void setNombrePuntoVenta(String nombrePuntoVenta) {
        this.nombrePuntoVenta = nombrePuntoVenta;
    }

    public String getDireccionPuntoVenta() {
        return direccionPuntoVenta;
    }

    public void setDireccionPuntoVenta(String direccionPuntoVenta) {
        this.direccionPuntoVenta = direccionPuntoVenta;
    }

    public ArrayList<Pedido> getPedidos() {
        return Pedidos;
    }

    public void setPedidos(ArrayList<Pedido> Pedidos) {
        this.Pedidos = Pedidos;
    }

    public PuntoVenta(int idPuntoVenta, String nombrePuntoVenta, String direccionPuntoVenta) {
        this.idPuntoVenta = idPuntoVenta;
        this.nombrePuntoVenta = nombrePuntoVenta;
        this.direccionPuntoVenta = direccionPuntoVenta;
    }
}
