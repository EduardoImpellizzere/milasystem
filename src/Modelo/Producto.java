/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author eduardo.impellizzere
 */
public class Producto {
    private int idProducto;
    private String nombreProducto;
    private String descripcionProducto;
    private int codigoProducto;
    private int stockProducto;
    private double precioProducto;
    private Rubro rubroProducto;
    private ArrayList<Agregado> AgregadosProducto = new ArrayList<>();

    public Producto() {
    }

    public Producto(int idProducto, String nombreProducto, String descripcionProducto, int codigoProducto, int stockProducto, double precioProducto, Rubro rubroProducto) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.descripcionProducto = descripcionProducto;
        this.codigoProducto = codigoProducto;
        this.stockProducto = stockProducto;
        this.precioProducto = precioProducto;
        this.rubroProducto = rubroProducto;
    }

    public Producto(int idProducto, String nombreProducto, String descripcionProducto, int codigoProducto, int stockProducto, Rubro rubroProducto) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.descripcionProducto = descripcionProducto;
        this.codigoProducto = codigoProducto;
        this.stockProducto = stockProducto;
        this.rubroProducto = rubroProducto;
    }

    @Override
    public String toString() {
        return "Producto{" + "idProducto=" + idProducto + ", nombreProducto=" + nombreProducto + ", descripcionProducto=" + descripcionProducto + ", codigoProducto=" + codigoProducto + ", stockProducto=" + stockProducto + ", precioProducto=" + precioProducto + ", rubroProducto=" + rubroProducto + ", AgregadosProducto=" + AgregadosProducto + '}';
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public int getStockProducto() {
        return stockProducto;
    }

    public void setStockProducto(int stockProducto) {
        this.stockProducto = stockProducto;
    }

    public double getPrecioProducto() {
        return precioProducto;
    }

    public void setPrecioProducto(double precioProducto) {
        this.precioProducto = precioProducto;
    }

    public Rubro getRubroProducto() {
        return rubroProducto;
    }

    public void setRubroProducto(Rubro rubroProducto) {
        this.rubroProducto = rubroProducto;
    }

    public ArrayList<Agregado> getAgregadosProducto() {
        return AgregadosProducto;
    }

    public void setAgregadosProducto(ArrayList<Agregado> AgregadosProducto) {
        this.AgregadosProducto = AgregadosProducto;
    }
}
