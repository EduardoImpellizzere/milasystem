/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author eduardo.impellizzere
 */
public class LineaPedido {
    private int idLineaPedido;
    private int cantidadLineaPedido;
    public ArrayList<Producto> LineasPedido = new ArrayList<>();

    public LineaPedido() {
    }

    @Override
    public String toString() {
        return "LineaPedido{" + "idLineaPedido=" + idLineaPedido + ", cantidadLineaPedido=" + cantidadLineaPedido + ", LineasPedido=" + LineasPedido + '}';
    }

    public int getIdLineaPedido() {
        return idLineaPedido;
    }

    public void setIdLineaPedido(int idLineaPedido) {
        this.idLineaPedido = idLineaPedido;
    }

    public int getCantidadLineaPedido() {
        return cantidadLineaPedido;
    }

    public void setCantidadLineaPedido(int cantidadLineaPedido) {
        this.cantidadLineaPedido = cantidadLineaPedido;
    }

    public ArrayList<Producto> getLineasPedido() {
        return LineasPedido;
    }

    public void setLineasPedido(ArrayList<Producto> LineasPedido) {
        this.LineasPedido = LineasPedido;
    }

    public LineaPedido(int idLineaPedido, int cantidadLineaPedido) {
        this.idLineaPedido = idLineaPedido;
        this.cantidadLineaPedido = cantidadLineaPedido;
    }
}
