/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author eduardo.impellizzere
 */
public class Turno {
    private int idTurno;
    private String nombreTurno;
    private String horaInicioTurno;
    private String horaFinalTurno;

    @Override
    public String toString() {
        return "Turno{" + "idTurno=" + idTurno + ", nombreTurno=" + nombreTurno + ", horaInicioTurno=" + horaInicioTurno + ", horaFinalTurno=" + horaFinalTurno + '}';
    }

    public int getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(int idTurno) {
        this.idTurno = idTurno;
    }

    public String getNombreTurno() {
        return nombreTurno;
    }

    public void setNombreTurno(String nombreTurno) {
        this.nombreTurno = nombreTurno;
    }

    public String getHoraInicioTurno() {
        return horaInicioTurno;
    }

    public void setHoraInicioTurno(String horaInicioTurno) {
        this.horaInicioTurno = horaInicioTurno;
    }

    public String getHoraFinalTurno() {
        return horaFinalTurno;
    }

    public void setHoraFinalTurno(String horaFinalTurno) {
        this.horaFinalTurno = horaFinalTurno;
    }

    public Turno(int idTurno, String nombreTurno, String horaInicioTurno, String horaFinalTurno) {
        this.idTurno = idTurno;
        this.nombreTurno = nombreTurno;
        this.horaInicioTurno = horaInicioTurno;
        this.horaFinalTurno = horaFinalTurno;
    }
}
