/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author eduardo.impellizzere
 */
public abstract class Persona {
    protected String nombrePersona;
    protected String apellidoPersona;
    protected String telefonoPersona;
    protected String domicilioPersona;

    @Override
    public String toString() {
        return "Persona{" + "nombrePersona=" + nombrePersona + ", apellidoPersona=" + apellidoPersona + ", telefonoPersona=" + telefonoPersona + ", domicilioPersona=" + domicilioPersona + '}';
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getApellidoPersona() {
        return apellidoPersona;
    }

    public void setApellidoPersona(String apellidoPersona) {
        this.apellidoPersona = apellidoPersona;
    }

    public String getTelefonoPersona() {
        return telefonoPersona;
    }

    public void setTelefonoPersona(String telefonoPersona) {
        this.telefonoPersona = telefonoPersona;
    }

    public String getDomicilioPersona() {
        return domicilioPersona;
    }

    public void setDomicilioPersona(String domicilioPersona) {
        this.domicilioPersona = domicilioPersona;
    }

    public Persona(String nombrePersona, String apellidoPersona, String telefonoPersona, String domicilioPersona) {
        this.nombrePersona = nombrePersona;
        this.apellidoPersona = apellidoPersona;
        this.telefonoPersona = telefonoPersona;
        this.domicilioPersona = domicilioPersona;
    }
}
