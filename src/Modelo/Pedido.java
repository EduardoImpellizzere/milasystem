/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author eduardo.impellizzere
 */
public class Pedido {
    private int idPedido;
    private String fechaPedido;
    private String horaPedido;
    private int tipoComprobantePedido;
    private ArrayList<LineaPedido> LineasPedidoPedido = new ArrayList<>();
    private Cajero cajeroPedido;

    @Override
    public String toString() {
        return "Pedido{" + "idPedido=" + idPedido + ", fechaPedido=" + fechaPedido + ", horaPedido=" + horaPedido + ", tipoComprobantePedido=" + tipoComprobantePedido + ", LineasPedidoPedido=" + LineasPedidoPedido + ", cajeroPedido=" + cajeroPedido + '}';
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public String getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(String fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public String getHoraPedido() {
        return horaPedido;
    }

    public void setHoraPedido(String horaPedido) {
        this.horaPedido = horaPedido;
    }

    public int getTipoComprobantePedido() {
        return tipoComprobantePedido;
    }

    public void setTipoComprobantePedido(int tipoComprobantePedido) {
        this.tipoComprobantePedido = tipoComprobantePedido;
    }

    public ArrayList<LineaPedido> getLineasPedidoPedido() {
        return LineasPedidoPedido;
    }

    public void setLineasPedidoPedido(ArrayList<LineaPedido> LineasPedidoPedido) {
        this.LineasPedidoPedido = LineasPedidoPedido;
    }

    public Cajero getCajeroPedido() {
        return cajeroPedido;
    }

    public void setCajeroPedido(Cajero cajeroPedido) {
        this.cajeroPedido = cajeroPedido;
    }

    public Pedido(int idPedido, String fechaPedido, String horaPedido, int tipoComprobantePedido, Cajero cajeroPedido) {
        this.idPedido = idPedido;
        this.fechaPedido = fechaPedido;
        this.horaPedido = horaPedido;
        this.tipoComprobantePedido = tipoComprobantePedido;
        this.cajeroPedido = cajeroPedido;
    }
}
