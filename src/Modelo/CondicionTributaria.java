/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author eduardo.impellizzere
 */
public class CondicionTributaria {
    private int idCondicionTributaria;
    private String tipoCondicionTributaria;

    @Override
    public String toString() {
        return "CondicionTributaria{" + "idCondicionTributaria=" + idCondicionTributaria + ", tipoCondicionTributaria=" + tipoCondicionTributaria + '}';
    }

    public int getIdCondicionTributaria() {
        return idCondicionTributaria;
    }

    public void setIdCondicionTributaria(int idCondicionTributaria) {
        this.idCondicionTributaria = idCondicionTributaria;
    }

    public String getTipoCondicionTributaria() {
        return tipoCondicionTributaria;
    }

    public void setTipoCondicionTributaria(String tipoCondicionTributaria) {
        this.tipoCondicionTributaria = tipoCondicionTributaria;
    }

    public CondicionTributaria(int idCondicionTributaria, String tipoCondicionTributaria) {
        this.idCondicionTributaria = idCondicionTributaria;
        this.tipoCondicionTributaria = tipoCondicionTributaria;
    }
}
