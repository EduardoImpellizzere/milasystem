/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author eduardo.impellizzere
 */
public class Agregado {
    private int idAgregado;
    private double precioAgregado;
    private int esOpcionalAgregado;
    private Producto detalleAgregado;

    @Override
    public String toString() {
        return "Agregado{" + "idAgregado=" + idAgregado + ", precioAgregado=" + precioAgregado + ", esOpcionalAgregado=" + esOpcionalAgregado + ", detalleAgregado=" + detalleAgregado + '}';
    }

    public int getIdAgregado() {
        return idAgregado;
    }

    public void setIdAgregado(int idAgregado) {
        this.idAgregado = idAgregado;
    }

    public double getPrecioAgregado() {
        return precioAgregado;
    }

    public void setPrecioAgregado(double precioAgregado) {
        this.precioAgregado = precioAgregado;
    }

    public int getEsOpcionalAgregado() {
        return esOpcionalAgregado;
    }

    public void setEsOpcionalAgregado(int esOpcionalAgregado) {
        this.esOpcionalAgregado = esOpcionalAgregado;
    }

    public Producto getDetalleAgregado() {
        return detalleAgregado;
    }

    public void setDetalleAgregado(Producto detalleAgregado) {
        this.detalleAgregado = detalleAgregado;
    }

    public Agregado(int idAgregado, double precioAgregado, int esOpcionalAgregado, Producto detalleAgregado) {
        this.idAgregado = idAgregado;
        this.precioAgregado = precioAgregado;
        this.esOpcionalAgregado = esOpcionalAgregado;
        this.detalleAgregado = detalleAgregado;
    }
}
