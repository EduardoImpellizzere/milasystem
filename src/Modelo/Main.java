package Modelo;

import Vista.InicioSesion;
import Datos.Generador;

public class Main {
    
    public static void main(String[] args) {    
        Generador.datos = new Generador().generarDatos();
//        System.out.println(Generador.datos.get(0)); //Turno
//        System.out.println(Generador.datos.get(1).toString()); //Rubro
//        System.out.println(Generador.datos.get(2)); //Documento
//        System.out.println(Generador.datos.get(3)); //CondTributaria
//        System.out.println(Generador.datos.get(4)); //Cajero
//        System.out.println(Generador.datos.get(5)); //Cliente
//        System.out.println(Generador.datos.get(6)); //Producto
//        System.out.println(Generador.datos.get(7)); //Agregado
//        System.out.println(Generador.datos.get(8)); //PtoVenta
//        System.out.println(Generador.datos.get(9)); //Pedido
//        System.out.println(Generador.datos.get(10)); //Empresa
        new InicioSesion().setVisible(true);
    }
    
}