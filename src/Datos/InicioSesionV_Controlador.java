/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Vista.InicioSesion;
import Vista.MenuPrincipal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author eduardo.impellizzere
 */
public class InicioSesionV_Controlador implements ActionListener{
    private InicioSesion ventana;

    public InicioSesionV_Controlador(InicioSesion ventana) {
        this.ventana = ventana;
    }

    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getActionCommand().equals("Continuar")){
            if (new UsuarioControlador().confirmarPass(this.ventana.jPasswordField1.getText()) == true){
                this.ventana.dispose();
                new MenuPrincipal().setVisible(true);
            }
            else {
                this.ventana.jPasswordField1.setText("");
            }
        }
        if(e.getActionCommand().equals("1")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "1");
        }
        if(e.getActionCommand().equals("2")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "2");
        }
        if(e.getActionCommand().equals("3")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "3");
        }
        if(e.getActionCommand().equals("4")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "4");
        }
        if(e.getActionCommand().equals("5")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "5");
        }
        if(e.getActionCommand().equals("6")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "6");
        }
        if(e.getActionCommand().equals("7")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "7");
        }
        if(e.getActionCommand().equals("8")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "8");
        }
        if(e.getActionCommand().equals("9")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "9");
        }
        if(e.getActionCommand().equals("0")){
            this.ventana.jPasswordField1.setText(this.ventana.jPasswordField1.getText() + "0");
        }
    }
}
