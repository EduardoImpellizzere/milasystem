/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Agregado;
import Modelo.Cajero;
import Modelo.Cliente;
import Modelo.CondicionTributaria;
import Modelo.Documento;
import Modelo.Empresa;
import Modelo.LineaPedido;
import Modelo.Pedido;
import Modelo.Producto;
import Modelo.PuntoVenta;
import Modelo.Rubro;
import Modelo.Turno;
import java.util.ArrayList;

/**
 *
 * @author eduardo.impellizzere
 */
public class Generador {
    public static ArrayList<Object> datos = new ArrayList<>();
    public static LineaPedido lineaPedido = new LineaPedido();
    
    public ArrayList generarDatos(){
        //Turnos
        Turno turno1 = new Turno(1,"Turno Mañana","8 AM","12 PM");
        Turno turno2 = new Turno(2,"Turno Tarde","1 PM","8 PM");
        
        ArrayList<Turno> arrayTurno = new ArrayList<>();
        arrayTurno.add(turno1);
        arrayTurno.add(turno2);
        
        //Rubros
        Rubro rubro1 = new Rubro(1,"Sandwich");
        Rubro rubro2 = new Rubro(2,"Agregados");
        Rubro rubro3 = new Rubro(3,"Bebidas");
        
        ArrayList<Rubro> arrayRubro = new ArrayList<>();
        arrayRubro.add(rubro1);
        arrayRubro.add(rubro2);
        arrayRubro.add(rubro3);
        
        //Documentos
        Documento documento1 = new Documento(1,"DNI",10,38185128);
        Documento documento2 = new Documento(2,"DNI",10,38054621);
        Documento documento3 = new Documento(3,"DNI",10,38216548);
        
        ArrayList<Documento> arrayDocumento = new ArrayList<>();
        arrayDocumento.add(documento1);
        arrayDocumento.add(documento2);
        arrayDocumento.add(documento3);
        
        //Condicion Tributaria
        CondicionTributaria condicionTributaria1 = new CondicionTributaria(1,"Monotributo");
        
        ArrayList<CondicionTributaria> arrayCondicionTributaria = new ArrayList<>();
        arrayCondicionTributaria.add(condicionTributaria1);
        
        //Cajeros
        Cajero cajero1 = new Cajero(1,100,"1411",turno1,"nombreCajero1","apellidoCajero1","telefonoCajero1","domicilioCajero1");
        Cajero cajero2 = new Cajero(2,200,"1411",turno2,"nombreCajero2","apellidoCajero2","telefonoCajero2","domicilioCajero2");
        
        ArrayList<Cajero> arrayCajero = new ArrayList<>();
        arrayCajero.add(cajero1);
        arrayCajero.add(cajero2);
        
        //Clientes
        Cliente cliente1 = new Cliente(1,documento1,condicionTributaria1,"nombreCliente1","apellidoCliente1","telefonoCliente1","domicilioCliente1");
        Cliente cliente2 = new Cliente(2,documento2,condicionTributaria1,"nombreCliente1","apellidoCliente1","telefonoCliente1","domicilioCliente1");
        Cliente cliente3 = new Cliente(3,documento3,condicionTributaria1,"nombreCliente1","apellidoCliente1","telefonoCliente1","domicilioCliente1");
        
        ArrayList<Cliente> arrayCliente = new ArrayList<>();
        arrayCliente.add(cliente1);
        arrayCliente.add(cliente2);
        arrayCliente.add(cliente3);
        
        //Productos
        Producto producto1 = new Producto(1,"Milanesa","Sandwich de Milanesa",1,50,80.00,rubro1);
        Producto producto2 = new Producto(2,"Lomito","Sandwich de Lomito",2,0,100.00,rubro1);
        Producto producto3 = new Producto(3,"Hamburguesa","Hamburguesa",3,50,80.00,rubro1);
        Producto producto4 = new Producto(4,"Jamon","Agregado de Jamon",4,50,rubro2);
        Producto producto5 = new Producto(5,"Queso","Agregado de Queso",5,50,rubro2);
        Producto producto6 = new Producto(6,"Huevo","Agregado de Huevo",6,50,rubro2);
        Producto producto7 = new Producto(7,"Lechuga","Agregado de Lechuga",7,50,rubro2);
        Producto producto8 = new Producto(8,"Tomate","Agregado de Tomate",8,50,rubro2);
        Producto producto9 = new Producto(9,"Mayonesa","Agregado de Mayonesa",9,50,rubro2);
        Producto producto10 = new Producto(10,"Savora","Agregado de Savora",10,50,rubro2);
        Producto producto11 = new Producto(11,"Gaseosa","Bebida Gaseosa",11,50,60.00,rubro3);
        Producto producto12 = new Producto(12,"Cerveza","Bebida Alcoholica",12,50,50.00,rubro3);
        
        ArrayList<Producto> arrayProducto = new ArrayList<>();
        arrayProducto.add(producto1);
        arrayProducto.add(producto2);
        arrayProducto.add(producto3);
        arrayProducto.add(producto4);
        arrayProducto.add(producto5);
        arrayProducto.add(producto6);
        arrayProducto.add(producto7);
        arrayProducto.add(producto8);
        arrayProducto.add(producto9);
        arrayProducto.add(producto10);
        arrayProducto.add(producto11);
        arrayProducto.add(producto12);
        
        //Agregados
        Agregado agregado1 = new Agregado(1,15.00,0,producto4);
        Agregado agregado2 = new Agregado(2,15.00,0,producto5);
        Agregado agregado3 = new Agregado(3,15.00,0,producto6);
        Agregado agregado4 = new Agregado(4,0.00,1,producto7);
        Agregado agregado5 = new Agregado(5,0.00,1,producto8);
        Agregado agregado6 = new Agregado(6,0.00,1,producto9);
        Agregado agregado7 = new Agregado(7,0.00,1,producto10);
        
        ArrayList<Agregado> arrayAgregado = new ArrayList<>();
        arrayAgregado.add(agregado1);
        arrayAgregado.add(agregado2);
        arrayAgregado.add(agregado3);
        arrayAgregado.add(agregado4);
        arrayAgregado.add(agregado5);
        arrayAgregado.add(agregado6);
        arrayAgregado.add(agregado7);
        
        //Punto de Venta
        PuntoVenta puntoVenta1 = new PuntoVenta(1,"nombrePuntoVenta1","direccionPuntoVenta1");
        
        ArrayList<PuntoVenta> arrayPuntoVenta = new ArrayList<>();
        arrayPuntoVenta.add(puntoVenta1);
        
        //Pedido
        Pedido pedido1 = new Pedido(1,"fechaPedido1","horaPedido1",1,cajero1);
        Pedido pedido2 = new Pedido(2,"fechaPedido2","horaPedido2",1,cajero1);
        Pedido pedido3 = new Pedido(3,"fechaPedido3","horaPedido3",1,cajero1);
        Pedido pedido4 = new Pedido(4,"fechaPedido4","horaPedido4",1,cajero2);
        Pedido pedido5 = new Pedido(5,"fechaPedido5","horaPedido5",1,cajero2);
        
        ArrayList<Pedido> arrayPedido = new ArrayList<>();
        arrayPedido.add(pedido1);
        arrayPedido.add(pedido2);
        arrayPedido.add(pedido3);
        arrayPedido.add(pedido4);
        arrayPedido.add(pedido5);
        
        //Empresa
        Empresa empresa1 = new Empresa(condicionTributaria1);
        
        ArrayList<Empresa> arrayEmpresa = new ArrayList<>();
        arrayEmpresa.add(empresa1);
        
        ArrayList<Object> datos = new ArrayList<>();
        datos.add(arrayTurno);
        datos.add(arrayRubro);
        datos.add(arrayDocumento);
        datos.add(arrayCondicionTributaria);
        datos.add(arrayCajero);
        datos.add(arrayCliente);
        datos.add(arrayProducto);
        datos.add(arrayAgregado);
        datos.add(arrayPuntoVenta);
        datos.add(arrayPedido);
        datos.add(arrayEmpresa);
        
        return datos;
    }
}
