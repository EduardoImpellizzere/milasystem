package Datos;

import Modelo.Producto;
import java.util.ArrayList;

public class ProductoControlador {
    public boolean controlarStock(int cod){
        ArrayList<Producto> productos = new ArrayList<>();
        productos = (ArrayList<Producto>) Generador.datos.get(6);
        
        for(Producto p1 : productos){
            if( cod == p1.getCodigoProducto() ){
                if(p1.getStockProducto() > 0)
                    return true;
            }
        }
        return false;
    }
    public void agregarProducto(int cod, int cant){
        ArrayList<Producto> productos = new ArrayList<>();
        productos = (ArrayList<Producto>) Generador.datos.get(6);
        
        for(Producto p1 : productos){
            if( cod == p1.getCodigoProducto() ){
                Generador.lineaPedido.LineasPedido.add(p1);
                Generador.lineaPedido.setCantidadLineaPedido(cant);
            }
        }
    }
}
