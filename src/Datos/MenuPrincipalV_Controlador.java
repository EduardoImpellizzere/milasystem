package Datos;

import Vista.MenuPrincipal;
import Vista.NuevoPedido;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuPrincipalV_Controlador implements ActionListener{
    private MenuPrincipal ventana;

    public MenuPrincipalV_Controlador(MenuPrincipal ventana) {
        this.ventana = ventana;
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Pedido")){
            this.ventana.dispose();
            new NuevoPedido().setVisible(true);
        }
    }
}
