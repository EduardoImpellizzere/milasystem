package Datos;

import Vista.SeleccionSandwich;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SeleccionSandwichV_Controlador implements ActionListener{
    private SeleccionSandwich ventana;

    public SeleccionSandwichV_Controlador(SeleccionSandwich ventana) {
        this.ventana = ventana;
    }
    
    public void deshabilitarBotones(){
        //1 milanesa
        //2 lomito
        //3 hamburguesa
        if(!new ProductoControlador().controlarStock(1))
            this.ventana.milanesaBTN.setEnabled(false);
        if(!new ProductoControlador().controlarStock(2))
            this.ventana.lomitoBTN.setEnabled(false);
        if(!new ProductoControlador().controlarStock(3))
            this.ventana.hamburguesaBTN.setEnabled(false);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Milanesa")){
            new ProductoControlador().agregarProducto(1,3);
            this.ventana.dispose();
            //////////
        }
    }
    
}
