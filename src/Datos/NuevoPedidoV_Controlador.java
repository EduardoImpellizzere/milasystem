package Datos;

import Vista.NuevoPedido;
import Vista.SeleccionSandwich;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NuevoPedidoV_Controlador implements ActionListener{
    private NuevoPedido ventana;

    public NuevoPedidoV_Controlador(NuevoPedido ventana) {
        this.ventana = ventana;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("VentanaSandwich")){
            new SeleccionSandwich().setVisible(true);
        }
        if(e.getActionCommand().equals("")){
            this.ventana.productLabel.setText("ja");
        }
    }
    
}
